Instructions:
--------------------------
Follow the instructions in the same order

	
1.Use ant to create war file from the source. run the following command in project root - ant release .
2.Place the war file in webApps folder of tomcat.
3.Place "prompts" folder containing all the media files for this project inside webApps folder of tomcat.
4.In the welcome.vxml and nowelcome.vxml's make following changes:
	update the url's for application ("basePathUrl") and prompts location ("promptBasePath").
5.Please keep below configuration in server's context.xml

for mysql database:-
-----------------------
<Resource name="jdbc/mysql-vhsnd" auth="Container"
			type="javax.sql.DataSource" maxTotal="100" maxIdle="30"
			maxWaitMillis="10000" username="root" password="password"
			driverClassName="com.mysql.jdbc.Driver" url="jdbc:mysql://ip:port/VHSND" />
			
		required db Jar:- mysql-connector-java-5.1.6.jar		
			
for MariaDB database:-
-----------------------			
<Resource name="jdbc/mysql-vhsnd" auth="Container"
			type="javax.sql.DataSource" maxTotal="100" maxIdle="30"
			maxWaitMillis="10000" username="root" password="password"
			driverClassName="org.mariadb.jdbc.Driver" url="jdbc:mariadb://ip:port/VHSND" />

		required db Jar:- drizzle-jdbc-1.3.jar	

5. Create the following objects in the Database as root: 

CREATE DATABASE VHSND;

CREATE TABLE VHSND.USER_CALL_INFO(MSISDN 				VARCHAR(15),
							CALLED_NUMBER 		VARCHAR(20),
							LANGUAGE_CODE   	VARCHAR(5),
							CARD_NUMBER			VARCHAR(5),
							CALL_START_TIME 	TIMESTAMP,
							CALL_END_TIME 		TIMESTAMP,
							PRIMARY KEY(MSISDN,CALL_START_TIME)
							)ENGINE=InnoDB DEFAULT CHARSET=UTF8;
							
CREATE TABLE VHSND.USER_INFO(MSISDN 				VARCHAR(15),
							TIME_FIRST_CALL			TIMESTAMP,
							PRIMARY KEY(MSISDN)
							)ENGINE=InnoDB DEFAULT CHARSET=UTF8;

CREATE TABLE VHSND.WHITELIST (MSISDN 				VARCHAR(10),
							PRIMARY KEY(MSISDN)
							)ENGINE=InnoDB DEFAULT CHARSET=UTF8;

CREATE TABLE VHSND.THEMES(
							CARD_NUMBER			VARCHAR(5),
							THEME 		VARCHAR(50),
							PRIMARY KEY(CARD_NUMBER)
							)ENGINE=InnoDB DEFAULT CHARSET=UTF8;							
GRANT ALL PRIVILEGES ON *.* TO 'root'@'%' IDENTIFIED BY 'onmobile' WITH GRANT OPTION;
FLUSH PRIVILEGES;

							
6. New System

JDK

install jdk and modify JAVA_HOME and PATH variables
#find / -name java <-- to know the java home directory
#export JAVA_HOME=/usr/java/jdk1.7.0_21
#export PATH=/usr/java/jdk1.7.0_21/bin:$PATH
#source /root/.bash_profile 

apache tomcat

#wget http://www.us.apache.org/dist/tomcat/tomcat-7/v7.0.63/bin/apache-tomcat-7.0.63.tar.gz
#tar xzf apache-tomcat-7.0.63.tar.gz

set the listener port in server.xml
set the db configuration in context.xml																	