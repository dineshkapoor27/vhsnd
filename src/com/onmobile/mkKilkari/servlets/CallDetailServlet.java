package com.onmobile.mkKilkari.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.onmobile.mkKilkari.beans.Users;
import com.onmobile.mkKilkari.db.DBManager;
import com.onmobile.mkKilkari.utils.Utils;


public class CallDetailServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private Logger logger = Logger.getLogger(CallDetailServlet.class);

	public CallDetailServlet() {
		super();
	}

	@Override
	public void init() throws ServletException {

	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try{
			doProcess(request, response);
		}
		catch(Exception ex){
			logger.error("error in doget ",ex);
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try{
			doProcess(request, response);
		}
		catch(Exception ex){
			logger.error("error in dopost ",ex);
		}
	}

	protected void doProcess(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		logger.info("doprocess");
		String msisdn = request.getParameter("msisdn");
		String calledNumber = request.getParameter("calledNumber");
		String languageCode = request.getParameter("languageCode");
		String cardNumber = request.getParameter("cardNumber");		
		String callStartTime = request.getParameter("callStartTime");
		logger.debug("time received from telephony " +callStartTime);
		String callEndTime = request.getParameter("callEndTime");

		PrintWriter out = response.getWriter();
		StringBuffer sb = new StringBuffer();
		logger.debug("user call detail received from telephony:-"+"msisdn:"+msisdn+"\n"+
				", calledNumber:"+calledNumber+"\n"+
				", languageCode:"+languageCode+"\n"+
				", cardNumber:"+cardNumber+"\n"+
				", callStartTime:"+callStartTime+"\n"+
				", callEndTime:"+callEndTime);
		if(languageCode== null || languageCode.equalsIgnoreCase("")){
			languageCode = "NA";
		}
		if(cardNumber== null || cardNumber.equalsIgnoreCase("")){
			cardNumber = "NA";
		}
		if(calledNumber== null || calledNumber.equalsIgnoreCase("")){
			calledNumber = "NA";
		}

		if(!(msisdn == null||msisdn.equals("")||callStartTime == null||callStartTime.equals("")||callEndTime == null||callEndTime.equals(""))){
			
			Users user = new Users(msisdn, calledNumber, languageCode, cardNumber, callStartTime, callEndTime);
			try {			
				DBManager.insertUserCalldetail(user);
				//request.setAttribute("status", "success");
				out.write(sb.toString());
				out.close();
				logger.info("vxml exit: "+sb.toString());
			} catch(SQLException ex){
				//request.setAttribute("status", "failed");
				logger.error("Error in DB:",ex);
			}		
			catch (Exception ex) {
				//request.setAttribute("status", "failed");
				logger.error("user call detail insertion failed:",ex);			
			}
		}
		//request.setAttribute("status", "failed");
		request.setAttribute("status", "success");
		/*
		response.setContentType("application/xml+vxml");
		//Utils.appendVXMLHeader(sb);
		sb.append("<?xml version=\"1.0\"  encoding= \"UTF-8\" ?>\n"); 
		sb.append("<vxml version=\"2.0\"  xml:lang=\"hindi\" xmlns=\"http://www.w3.org/2001/vxml\">\n");
		sb.append("<property name=\"timeout\" value=\"12000ms\"/>\n");
		
		sb.append("<form>\n");
		sb.append("	<block>\n");
		sb.append("		<exit/>\n");
		sb.append("	</block>\n");
		sb.append("</form>\n");
		sb.append("</vxml>\n");	
		//Utils.appendVXMLFooter(sb);
		*/
		out.write(sb.toString());
		out.close();
		logger.info("vxml exit: "+sb.toString());
	}

}
