package com.onmobile.mkKilkari.servlets;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.net.URL;
import java.sql.SQLException;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.onmobile.mkKilkari.beans.UserInfo;
import com.onmobile.mkKilkari.beans.Users;
import com.onmobile.mkKilkari.db.DBManager;
import com.onmobile.mkKilkari.utils.Utils;


public class vhsndvxml extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private Logger logger = Logger.getLogger(vhsndvxml.class);
	static String welcomevxml = "";
	static String nowelcomevxml = "";

	public vhsndvxml() {
		super();
	}

	@Override
	public void init() throws ServletException {
		ServletContext context = this.getServletContext();
		InputStream is = context.getResourceAsStream("/welcome.vxml");
	    java.util.Scanner s = new java.util.Scanner(is).useDelimiter("\\A");
	    welcomevxml = s.hasNext() ? s.next() : "";
	    InputStream is2 = context.getResourceAsStream("/nowelcome.vxml");
	    java.util.Scanner s2 = new java.util.Scanner(is2).useDelimiter("\\A");
	    nowelcomevxml = s2.hasNext() ? s2.next() : "";
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try{
			doProcess(request, response);
		}
		catch(Exception ex){
			logger.error("error in doget ",ex);
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try{
			doProcess(request, response);
		}
		catch(Exception ex){
			logger.error("error in dopost ",ex);
		}
	}

	protected void doProcess(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		logger.info("doprocess");
		String msisdn = request.getParameter("callerId");
		String operator = request.getParameter("operator");
		String circle = request.getParameter("circle");
		String calledNumber = request.getParameter("calledNumber");
		String callId = request.getParameter("callId");
		logger.debug("user msisdn received from telephony:-msisdn:"+msisdn+
				"\n operator:" + operator +
				"\n circle:" + circle +
				"\n calledNumber:" + calledNumber +
				"\n callId:" + callId);

		PrintWriter out = response.getWriter();
		StringBuffer sb = new StringBuffer();
	/*	if(callStartTime== null || callStartTime.equalsIgnoreCase("")){
			callStartTime = "NA";
		}*/
		response.setContentType("application/xml+vxml");
		response.setHeader("Cache-control", "no-cache, no-store");
		response.setHeader("Pragma", "no-cache");
		response.setHeader("Expires", "-1");
		if((msisdn != null) && (!msisdn.equalsIgnoreCase("")))
		{
		UserInfo userInfo = new UserInfo(msisdn,false);
		try {			
			DBManager.checkUserStatus(userInfo);
			//request.setAttribute("status", "success");
			if(userInfo.getUserExists())
			{
				sb.append(nowelcomevxml);
			}
			else
			{
				DBManager.insertUserInfo(userInfo);
				sb.append(welcomevxml);
			}
			out.write(sb.toString());
			out.close();
			logger.info("vxml exit: "+sb.toString());
			return;
		} catch(SQLException ex){
			//request.setAttribute("status", "failed");
			logger.error("Error in DB:",ex);
		}		
		catch (Exception ex) {
			//request.setAttribute("status", "failed");
			logger.error("Error in DB:",ex);			
		}		
		}
		//request.setAttribute("status", "failed");
		request.setAttribute("status", "success");
		response.setContentType("application/xml+vxml");
		//Utils.appendVXMLHeader(sb);
		sb.append("<?xml version=\"1.0\"  encoding= \"UTF-8\" ?>\n"); 
		sb.append("<vxml version=\"2.0\"  xml:lang=\"hindi\" xmlns=\"http://www.w3.org/2001/vxml\">\n");
		sb.append("<property name=\"timeout\" value=\"12000ms\"/>\n");
		
		sb.append("<form>\n");
		sb.append("	<block>\n");
		sb.append("		<exit/>\n");
		sb.append("	</block>\n");
		sb.append("</form>\n");
		sb.append("</vxml>\n");	
		//Utils.appendVXMLFooter(sb);
		
		out.write(sb.toString());
		out.close();
		logger.info("vxml exit: "+sb.toString());
	}
}
