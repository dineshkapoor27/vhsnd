package com.onmobile.mkKilkari.beans;

import java.sql.Date;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;

import org.apache.log4j.Logger;

public class Users {
	private String msisdn;
	private String calledNumber;
	private String languageCode;
	private String cardNumber;
	private String callStartTime;
	private String callEndTime;
	Logger logger = Logger.getLogger(Users.class);
	
	public Users() {
		
	}

	public Users(String msisdn, String calledNumber, String languageCode,
			String cardNumber, String callStartTime, String callEndTime) {
		
		this.msisdn = msisdn;
		this.calledNumber = calledNumber;
		this.languageCode = languageCode;
		this.cardNumber = cardNumber;
		this.callStartTime = callStartTime;
		this.callEndTime = callEndTime;
	}

	public String getMsisdn() {
		return msisdn;
	}

	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}

	public String getCalledNumber() {
		return calledNumber;
	}

	public void setCalledNumber(String calledNumber) {
		this.calledNumber = calledNumber;
	}

	public String getLanguageCode() {
		return languageCode;
	}

	public void setLanguageCode(String languageCode) {
		this.languageCode = languageCode;
	}

	public String getCardNumber() {
		return cardNumber;
	}

	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}

	public Timestamp getCallStartTime() {
		Timestamp callStartTimeStamp = Timestamp.valueOf(callStartTime);
		logger.debug("call timestamp "+ callStartTimeStamp);
		return callStartTimeStamp;
		
	}

	public void setCallStartTime(String callStartTime) {
		this.callStartTime = callStartTime;
	}

	public Timestamp getCallEndTime() {	
		Timestamp callEndTimeStamp = Timestamp.valueOf(callEndTime);
		logger.debug("call end timestamp "+ callEndTimeStamp);
		return callEndTimeStamp;
	}

	public void setCallEndTime(String callEndTime) {
		this.callEndTime = callEndTime;
	}
	
//	public static void main(String[] args) {
//		String callStartTime = "1418813029";
//		long stm = Long.parseLong(callStartTime);
//		String text  = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.sql.Date(stm*1000));
//		Timestamp callStartTimeStamp = Timestamp.valueOf(text);
//		System.out.println("Converted time in sql timestamp is "+callStartTimeStamp);
//		java.util.Date utilDate = new java.util.Date();
//		java.sql.Date sqlDate = new java.sql.Date(stm*1000);
//		System.out.println("utilDate:" + utilDate);
//		System.out.println("sqlDate:" + sqlDate);
//	}
//	
}
