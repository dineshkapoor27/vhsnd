package com.onmobile.mkKilkari.beans;

import java.sql.Timestamp;

import org.apache.log4j.Logger;

public class UserInfo {	
	
private String msisdn;
private Boolean userExists;


Logger logger = Logger.getLogger(UserInfo.class);

public UserInfo() {
	
}

public UserInfo(String msisdn,Boolean userExists) {
	
	this.msisdn = msisdn;
	this.userExists = userExists;
}

public String getMsisdn() {
	return msisdn;
}

public void setMsisdn(String msisdn) {
	this.msisdn = msisdn;
}

public Boolean getUserExists() {
	return userExists;
}

public void setUserExists(Boolean userExists) {
	this.userExists = userExists;
}

}
