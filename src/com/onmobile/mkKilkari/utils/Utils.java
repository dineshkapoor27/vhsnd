package com.onmobile.mkKilkari.utils;
import  java.util.Scanner;

import org.apache.log4j.Logger;

import com.onmobile.mkKilkari.db.DBManager;

import java.io.File;

public class Utils {
	private  static Logger logger = Logger.getLogger(Utils.class);	
	static String welcomevxml = "";
	static String nowelcomevxml = "";
	public static void readVXML(){
		String appPath = System.getProperty("user.dir");
		try{
			welcomevxml = new Scanner(new File(appPath + "/welcome.vxml")).useDelimiter("\\Z").next();
			}
			catch(Exception e)
			{
				logger.error("welcomevxml not found.", e);
				System.out.println("No vxml deployed");
			};
		try{
						nowelcomevxml = new Scanner(new File(appPath + "/nowelcome.vxml")).useDelimiter("\\Z").next();
			}
			catch(Exception e)
			{
				logger.error("nowecomevxml not found.", e);
				System.out.println("No vxml deployed");
			};
			logger.error("Utils initialized");
			return;
	}
	
	public static void appendVXMLHeader(StringBuffer sb){
		sb.append("<?xml version=\"1.0\"  encoding= \"UTF-8\" ?>\n"); 
		sb.append("<vxml version=\"2.0\"  xml:lang=\"hindi\" xmlns=\"http://www.w3.org/2001/vxml\">\n");
		sb.append("<property name=\"timeout\" value=\"12000ms\"/>\n");
	}
	public static void appendVXMLFooter(StringBuffer sb){
		sb.append("</vxml>\n");
	}
	
	public static void appendNoWelcome(StringBuffer sb){
		sb= sb.append(nowelcomevxml);
	}

	public static void appendWelcome(StringBuffer sb){
		sb= sb.append(welcomevxml);
	}
	
	
}
