package com.onmobile.mkKilkari.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.naming.InitialContext;
import javax.sql.DataSource;

import org.apache.log4j.Logger;

import com.onmobile.mkKilkari.beans.UserInfo;
import com.onmobile.mkKilkari.beans.Users;

public class DBManager {

	private static DataSource dataSource = null;
	private static String dataSourceParam = null;
	private  static Logger logger = Logger.getLogger(DBManager.class);	


	static {
		System.out.println("Creating connection");
		try{			
			if(dataSourceParam==null||dataSourceParam.equalsIgnoreCase(""))
				dataSourceParam = "java:comp/env/jdbc/mysql-vhsnd";
			InitialContext context = new InitialContext();
			logger.debug("Going to lookup for data source param : " + dataSourceParam);
			dataSource = (DataSource) context.lookup(dataSourceParam);
		}
		catch(Exception e){
			logger.error("Exception while lookup for data source.", e);
		}
		System.out.println("db detail loaded...");
	}

	private static Connection openConnection(){

		Connection oConnection = null;
		try {
			oConnection = dataSource.getConnection();
		} catch (SQLException e) {
			logger.error("Exception while opening connection.",e);
		}
		logger.debug("openConnection:returning " + oConnection);
		System.out.println("connection returned");
		return oConnection ;
	}


	private  static void closeConnection(Connection oConnection){		
		try{
			if(oConnection != null){
				oConnection.close();
				logger.debug("Connection closed successfully.");
			}
		}
		catch(Exception e){
			logger.error("Exception while closing connection.", e);
		}
	}

	public static boolean insertUserCalldetail(Users user) throws Exception{
		boolean status = false;
		Connection con = null;
		PreparedStatement pstmt = null;
		String sqlQuery = "INSERT INTO USER_CALL_INFO VALUE (?,?,?,?,?,?)";
		try{
			con = DBManager.openConnection();	
			logger.debug("got connection from datasource");
			logger.debug("MSISDN "+user.getMsisdn()+", Shortcode "+ user.getCalledNumber()+", Language code " +user.getLanguageCode()+
					", Card number "+user.getCardNumber()+ ", call Start time "+user.getCallStartTime()+ ", call end time " +user.getCallEndTime());
			pstmt = con.prepareStatement(sqlQuery);
			pstmt.setString(1, user.getMsisdn());
			pstmt.setString(2, user.getCalledNumber());
			pstmt.setString(3, user.getLanguageCode());
			pstmt.setString(4, user.getCardNumber());
			pstmt.setTimestamp(5, user.getCallStartTime());
			pstmt.setTimestamp(6, user.getCallEndTime());
			int i = pstmt.executeUpdate();
			if(i==1){
				status = true;
				logger.info("User call detail inseted to DB: ");
			}
			else{
				logger.error("DB insertion failed: "+String.valueOf(user));
				throw new Exception("Data insertion failed");				
			}
		}
		catch(SQLException ex){			
			throw ex;
		}
		catch(Exception ex){
			throw ex;
		}		
		finally{
			if(con != null){
				DBManager.closeConnection(con);
			}
		}
		return status;
	}
	
	public static boolean insertUserInfo(UserInfo userInfo) throws Exception{
		boolean status = false;
		Connection con = null;
		PreparedStatement pstmt = null;
		String sqlQuery = "";
			sqlQuery = "INSERT INTO USER_INFO VALUE (?,NOW())";
		try{
			con = DBManager.openConnection();	
			logger.debug("got connection from datasource");
			logger.debug("MSISDN "+userInfo.getMsisdn());
			pstmt = con.prepareStatement(sqlQuery);
			pstmt.setString(1, userInfo.getMsisdn());
			int i = pstmt.executeUpdate();
			if(i==1){
				status = true;
				logger.info("User info inserted to DB: ");
			}
			else{
				logger.error("DB insertion failed");
				throw new Exception("Data insertion failed");				
			}
		}
		catch(SQLException ex){			
			throw ex;
		}
		catch(Exception ex){
			throw ex;
		}		
		finally{
			if(con != null){
				DBManager.closeConnection(con);
			}
		}
		return status;
	}
	
	public static boolean checkUserStatus(UserInfo uInfo) throws Exception{
		boolean status = false;
		Connection con = null;
		PreparedStatement pstmt = null;
		logger.debug("MSISDN = " + uInfo.getMsisdn());
		String sqlQuery = "SELECT COUNT(*) AS COUNT FROM USER_INFO WHERE MSISDN = " + uInfo.getMsisdn();
		try{
			con = DBManager.openConnection();	
			pstmt = con.prepareStatement(sqlQuery);
			ResultSet rs = pstmt.executeQuery();
			int count = 0;
			while (rs.next()){
			count = rs.getInt("count");}
			
			if(count == 0){
				uInfo.setUserExists(false);
				logger.info("New user");
			}
			else{
				uInfo.setUserExists(true);
				logger.info("Old user");
			}
		}
		catch(SQLException ex){			
			throw ex;
		}
		catch(Exception ex){
			throw ex;
		}		
		finally{
			if(con != null){
				DBManager.closeConnection(con);
			}
		}
		return true;
	}
}