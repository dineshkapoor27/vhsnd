<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE ccxml SYSTEM "ccxml.dtd">
<%@page contentType="application/ccxml+xml"%><%
		response.setHeader("Cache-Control","public");
		response.setHeader("Cache-Control","max-age="+(1*30*60));%>

<%
	String applicationUrl = "http://10.155.8.116:9999/KilkariMKDemo";
	String vhsndDialogUrl = applicationUrl+"/vhsndvxml.vxml";
	String CallDetailServletUrl = applicationUrl+"/CallDetailServlet";
%>

<ccxml version="1.0" xmlns="http://www.w3.org/2002/09/ccxml"> 

	<!-- Lets declare our state var -->
	<!-- String vhsndDialogUrl = applicationUrl+"/vhsndvxml.vxml" -->
  	<var name="state0" expr="'init'"/>
	
	<var name ="calledNumber" expr = "''"/>
  	<var name ="callerId" expr = "''"/>
	<var name ="callId" expr = "''"/>
	<var name ="startTime" expr = "new Date().getTime()"/>
	<var name ="language" expr = "''"/>
	<var name ="promptInfo" expr = "''"/>
  	<var name ="inconnectionid" expr="''" />
	<var  name="appName" expr="'vhsnd'"/>
	<var name ="isDialogRunning" expr="false" />
	<var name="sessionId" expr="''" />
	<var name ="basichttp" expr="''"/>
	<var name ="appDisc" expr="''" />
	<var name ="applicationType" expr="''" />

	<var name="applicationUrl" expr="'<%=applicationUrl%>'"/>	
	<var name="vhsndDialogUrl" expr="'<%=vhsndDialogUrl%>'"/>
	<var name="submitUrl" expr="'<%=CallDetailServletUrl%>'"/>
	
	<var name="nextDialogUrl" expr="''" />
	<var name="dialogId" expr="''" />
	
	<var name="msisdn" expr="callerId"/>
	<var name="languageCode" expr="''" />
	<var name="cardNumber" expr="''" />
	<var name="callStartTime" expr="''" />
	<var name="callEndTime" expr="''" />

	
	<eventprocessor statevariable="state0">
	   	<transition state="init" event="connection.alerting">
    		<!-- Assigning Events Variables -->
    		<assign name="calledNumber" expr="event$.connection.local"/>
    		<assign name="callerId" expr="event$.connection.remote"/>
    		<assign name="inconnectionid" expr="event$.connectionid"/>
			<assign name="sessionId" expr="session.id"/>
			<assign name="basichttp" expr="session.ioprocessors.basichttp"/>
			<assign name="callId" expr="callerId + '-' + (new Date()).valueOf()"/>
			<assign name="startTime" expr="(new Date()).valueOf()"/>
      		<!--  Printing Events Variables -->
      		<log expr = "'Answering incoming call from '+callerId+' to number '+calledNumber"/>
			<!-- Check calledNumber Number/dummy check/ the calls are accepted in the else block -->
			<if cond="calledNumber==5771101 || calledNumber==5771111">
					<accept/>
			<elseif cond="calledNumber==57711"/>
				<log expr="'dial a wrong number so rejecting the call.'"/>
				<reject/>
			<else/>
				<accept/>
			</if>
		</transition>
		
		<!-- Call has been answered -->
		<transition state="init" event="connection.connected"> 
			<log expr="'Call is connected. Connection id is : '+event$.connectionid"/>
			<log expr="'vhsnd vxml url is '+vhsndDialogUrl"/>
			<dialogstart src="vhsndDialogUrl" dialogid="vhsndDialogId" namelist="callerId operator circle calledNumber callId" connectionid="inconnectionid"/>
		 	<assign name="state0" expr="'dialogActive'"/>
		</transition> 
		
		 <transition event="dialog.started">
		    <log expr = "'current dialog is started id:'+event$.dialogid"/>
			<assign name="isDialogRunning" expr="true"/>
	 	</transition>
	 	
		<!-- Process the dialog exit -->  	 	
	    <transition event="dialog.exit">
			<log expr="'Dialog that was exited now is '+event$.dialogid" />
			<assign name="isDialogRunning" expr="false"/>
			<assign name="appDisc" expr="event$.values.appDisc"/>
		<!--	<if cond="appDisc!=null">
				<else/>
				<assign name="nextDialogUrl" expr="applicationUrl+event$.values.nextDialogUrl"/>
				<log expr="'nextDialogUrl vxml url is '+nextDialogUrl"/>
				<if cond="event$.dialogid==vhsndDialogId">
					<log expr="'Dialog that was exited now is vhsnd. so doing a submit'" />
					<assign name="msisdn" expr="event$.values.msisdn"/>
					<assign name="calledNumber" expr="event$.values.calledNumber"/>
					<assign name="languageCode" expr="event$.values.languageCode"/>
					<assign name="cardNumber" expr="event$.values.cardNumber"/>
					<assign name="callStartTime" expr="event$.values.callStartTime"/>
					<assign name="callEndTime" expr="event$.values.callEndTime"/>
					<send target="submitUrl" targettype ="'basichttp'" namelist ="msisdn calledNumber languageCode cardNumber callStartTime callEndTime"/>	
				</if> 
			</if>-->
		<disconnect/>
		<exit/>
	    </transition>
		
		<!-- Caller hung up. Lets just go on and end the session -->
    	<transition event="connection.disconnected">
    		<log expr = "'user with callerId = '+callerId+' has disconnected.'"/>
			<if cond="isDialogRunning==false">
				<log expr = "'no dialog is running so calling Temp DB data posting API.'"/>
			</if>
			<exit/>
    	</transition>
		
		<!-- Something went wrong with javascript. end the call -->
    	<transition event="error.semantic">
      		<log expr="'Error in processing the dialog : (' + event$.reason + ')'"/>
			<exit/>
    	</transition>
		
		 <!-- Something went wrong. append disconnect event and end the call -->
    	<transition event="error.*" >
      		<log expr="'Error in processing the dialog : (' + event$.reason + ')'"/>
			<exit/>
    	</transition>
		
	</eventprocessor> 
</ccxml>